import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';
import { NgModel, Validators } from '@angular/forms';
import { FormService } from '../form.service';

@Component({
  selector: 'app-recaptcha',
  templateUrl: './recaptcha.component.html',
  styleUrls: ['./recaptcha.component.scss']
})
export class RecaptchaComponent implements OnInit {

  form;
  domain;
  success;
  @ViewChild('captchaRef') public captchaRef: RecaptchaComponent;

  constructor(
    private api: ApiService,
    private toast: ToastService,
    protected formService: FormService,
    public config: ConfigService
  ) { }

  ngOnInit(): void {
    this.form = this.createForm();
    this.domain = document.location.hostname;
  }

  createForm() {
    return this.formService.createFormGroup({
      obj: {
        sample_field: ['text', [Validators.required]],
        "g-recaptcha-response": ['text', [Validators.required]],
      }
    });
  }

  submit() {
    if (!this.form.trySubmit()) return;

    this.api.post({
      endpoint: `${this.config.xanoApiUrl}/recaptcha/validate`,
      params: {
        ...this.form.value
      },
    })
    .subscribe((response:any) => {
      this.toast.success("Success");
      this.success = true;
    }, (err) => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
    });
  }
}
