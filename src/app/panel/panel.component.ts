import { OnInit, OnDestroy, Component } from '@angular/core';
import { PanelService } from '../panel.service';

@Component({
  templateUrl: './panel.component.html',
})
export abstract class PanelComponent implements OnInit, OnDestroy {
  panelSub: any;
  data;
  open;

  constructor(
    protected panel: PanelService,
  ) { }

  abstract getComponentId();

  ngOnInit() {
    this.panelSub = this.panel.observe()
    .subscribe(event => {
      if (event.key == this.getComponentId()) {
        if (event.open) {
          this.data = event.data;
          this.onOpen();
        } else {
          this.onClose();
        }
        this.open = event.open;
      }
    });

    this.open = false;
  }

  ngOnDestroy() {
    if (this.panelSub) {
      this.panelSub.unsubscribe();
    }
  }

  onOpen() {
  }

  onClose() {
  }

  close() {
    this.panel.close(this.getComponentId());
  }
}
